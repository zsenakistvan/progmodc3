package almafa;

import annotation.MinMaxValue;
import java.lang.reflect.Field;
import java.util.Arrays;

public class AlmaFa {
    public static void main(String[] args) {
      
        Alma a = new Alma();
        Alma l = new Alma();
        Alma m = new Alma(4, SZINEK.RED, IZEK.ÉDES, Boolean.TRUE);
        
        System.out.println(m.getSzin());
        System.out.println("Az m almám színe: " + m.getSzin().getValue());
        System.out.println("A programban a " + m.getSzin().getValue() + " szín kódja: " + m.getSzin().getKey());
        System.out.println(a.getIz());
        //a.iz = "Édes";
        //System.out.println(a.iz);
        System.out.println(l.getIz());
        System.out.println(m.getIz());
        //l.iz = "Borsízű";
        System.out.println(l.getIz());
        System.out.println(m.getMagSzam());
        
        System.out.println(m.getIz());
        m.setIz(IZEK.KESERŰ);
        System.out.println(m.getIz());
        m.setIz(IZEK.GEJL); 
        System.out.println(m.getIz().getJoIzSkalaErtek());
        System.out.println("Az m almám íze: " + m.getIz().normalizaltNev());
        
        //a.magSzam = -2;
        //System.out.println(a.magSzam);
        
        
        System.out.println(a.getMagSzam());
        a.pluszEgyMag();
        a.pluszMag(2);
        System.out.println(a.getMagSzam());
        a.pluszMag(20);
        System.out.println(a.getMagSzam());
        
        System.out.println(a.isErett());
        a.megerik();
        System.out.println(a.isErett());
        
        
        Alma p = new Alma(4, SZINEK.RED, IZEK.ÉDES, Boolean.TRUE);
        System.out.println(p.getMagSzam());
        Field[] fields = p.getClass().getDeclaredFields();
        Integer max = 999;
        for(Field f : fields){
            //System.out.println(f.getName());
            if(f.getName().equals("magSzam")){
                max = f.getAnnotation(MinMaxValue.class).max();
                //System.out.println("max: " + max);
            }
        }
        
        if(p.getMagSzam() < max){
            p.pluszEgyMag();
        }
        else{
            System.out.println("Nem lehet több magot növeszteni, mint tíz!");
        }
        System.out.println(p.getMagSzam());
        
        Funkciok<Alma> f = new Funkciok<Alma>();
        //f.mentes(a);
        
        Cseresznye cs = new Cseresznye("Meggy", SZINEK.RED, IZEK.SAVANYÚ, Boolean.FALSE);
        Funkciok<Cseresznye> k = new Funkciok<Cseresznye>();
        //k.mentes(cs);
        
        System.out.println(Arrays.toString(f.betoltes(a)));
        
    }
    
    
    
}