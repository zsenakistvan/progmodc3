package almafa;

import annotation.getterFunctionName;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;

public class Cseresznye extends Gyumolcs{
    @getterFunctionName(value = "getFajta", type = String.class)
    private String fajta;

    public Cseresznye() {
        super();
        this.fajta = "Alapcseresznye";
    }

    public Cseresznye(String fajta, AbstractMap.SimpleEntry<String, String> szin, IZEK iz, Boolean erett) {
        super(szin, iz, erett);
        this.fajta = fajta;
    }

    public String getFajta() {
        return this.fajta;
    }
    
    public static List<Cseresznye> getAllCseresznye(){
        Funkciok f = new Funkciok();
        Object[] egyedek = f.betoltes(new Cseresznye());
        return new ArrayList();//it kell folytatni....először konkrétan a cseresznye osztályra
    }
    
}
