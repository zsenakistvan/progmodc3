package almafa;
public enum IZEK {
    SAVANYÚ(7, "savanyú"), KESERŰ(1, "keserű"), ÉDES(10, "édes"), GEJL(4, "gejl"), ÍZTELEN(5, "íztelen");
    
    private Integer joIzSkalaErtek;
    private String kiirandoErtek;
    private IZEK(Integer jisk, String ke){
        this.joIzSkalaErtek = jisk;
        this.kiirandoErtek = ke;
    }

    public Integer getJoIzSkalaErtek() {
        return this.joIzSkalaErtek;
    }

    public String normalizaltNev() {
        return this.kiirandoErtek;
    }
    
    
    
    
}
