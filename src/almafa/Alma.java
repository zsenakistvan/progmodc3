package almafa;

import annotation.MinMaxValue;
import annotation.getterFunctionName;
import java.util.AbstractMap.SimpleEntry;

public class Alma extends Gyumolcs{
    @MinMaxValue(min = 0, max = 4)
    @getterFunctionName(value = "getMagSzam", type = Integer.class)
    private Integer magSzam;
    
    public Alma(){
        super();
        this.magSzam = 0;
    }

    public Alma(Integer magSzam, SimpleEntry<String, String> szin, IZEK iz, Boolean erett) {
        super(szin, iz, erett);
        this.magSzam = 0;
        try{
            if(magSzam <= Alma.class.getDeclaredField("magSzam").getAnnotation(MinMaxValue.class).max()){
                this.magSzam = magSzam;
            }
        }
        catch(Exception ex){
            System.out.println("Hiba: " + ex.toString());
        }
    }
    
    public Integer getMagSzam() {
        return this.magSzam;
    }

    public void pluszEgyMag(){
        //feltételezzük, hogy 10 magnál nem lehet több
        try{
            if(this.magSzam < Alma.class.getDeclaredField("magSzam").getAnnotation(MinMaxValue.class).max()) {
                this.magSzam++;
            }
        }
        catch(Exception ex){
            System.out.println("Hiba: " + ex.toString());
        }
    }
    
    public void pluszMag(Integer ujMagokSzama){
        try{
            if(this.magSzam + ujMagokSzama < Alma.class.getDeclaredField("magSzam").getAnnotation(MinMaxValue.class).max()){
                this.magSzam += ujMagokSzama;
            }
        }
        catch(Exception ex){
            System.out.println("Hiba: " + ex.toString());
        }
    }  
    
}
