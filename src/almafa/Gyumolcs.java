package almafa;

import annotation.getterFunctionName;
import java.util.AbstractMap;

public class Gyumolcs {
    @getterFunctionName(value = "getSzin", type = AbstractMap.SimpleEntry.class)
    protected AbstractMap.SimpleEntry<String, String> szin;
    @getterFunctionName(value = "getIz", type = IZEK.class)
    protected IZEK iz;
    @getterFunctionName(value = "isErett", type = Boolean.class)
    protected Boolean erett;

    public Gyumolcs() {
        this.erett = Boolean.FALSE;
        this.iz = IZEK.ÍZTELEN;
        this.szin = SZINEK.YEL;
    }

    public Gyumolcs(AbstractMap.SimpleEntry<String, String> szin, IZEK iz, Boolean erett) {
        this.szin = szin;
        this.iz = iz;
        this.erett = erett;
    }

    public AbstractMap.SimpleEntry<String, String> getSzin() {
        return this.szin;
    }

    public IZEK getIz() {
        return this.iz;
    }

    public Boolean isErett() {
        return this.erett;
    }
    
    public void megerik(){
        this.erett = Boolean.TRUE;
    }

    public void setSzin(AbstractMap.SimpleEntry<String, String> szin) {
        this.szin = szin;
    }

    public void setIz(IZEK iz) {
        this.iz = iz;
    }
    
    
    
    
    
    
    
}
